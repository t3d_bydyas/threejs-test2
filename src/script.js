import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'lil-gui'

THREE.ColorManagement.enabled = false


/**
 * Base
 */
// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()


/**
 * Galaxy
 */
const params = {
    count: 100_000,
    size: 0.01,
    radius: 4,
    branches: 3,
    spin: 1,
    randomness: 0.2,
    randomnessPower: 3,
    insideColor: '#ff6030',
    outsideColor: '#1b3984',
    raycastedArea: 1
}

let geometry = null
let material = null
let points = null

const generateGalaxy = () => {
    // Destroy old galaxy
    if (points !== null) {
        geometry.dispose()
        material.dispose()
        scene.remove(points)
    }

    // Geometry
    geometry = new THREE.BufferGeometry()
    const positions = new Float32Array(params.count * 3) // [x, y ,z]
    const colors = new Float32Array(params.count * 3) // [r, g, b]

    const colorInside = new THREE.Color(params.insideColor)
    const colorOutside = new THREE.Color(params.outsideColor)

    for (let i = 0; i < params.count; i++) {
        const i3 = i * 3
        
        // Position
        const radius = Math.random() * params.radius
        const branchAngle = (i % params.branches) / params.branches * Math.PI*2
        const spinAngle = radius * params.spin

        const randomX = Math.pow(Math.random(), params.randomnessPower) * (Math.random() < 0.5 ? 1 : -1)
        const randomY = Math.pow(Math.random(), params.randomnessPower) * (Math.random() < 0.5 ? 1 : -1)
        const randomZ = Math.pow(Math.random(), params.randomnessPower) * (Math.random() < 0.5 ? 1 : -1)

        positions[i3] = Math.cos(branchAngle + spinAngle) * radius + randomX
        positions[i3 + 1] = 0 + randomY
        positions[i3 + 2] = Math.sin(branchAngle + spinAngle) * radius + randomZ
        
        // Color
        const mixedColor = colorInside.clone()
        mixedColor.lerp(colorOutside, radius / params.radius)

        colors[i3] = mixedColor.r
        colors[i3 + 1] = mixedColor.g
        colors[i3 + 2] = mixedColor.b
    }

    geometry.setAttribute(
        'position', 
        new THREE.BufferAttribute(positions, 3)
    )

    geometry.setAttribute(
        'color', 
        new THREE.BufferAttribute(colors, 3)
    )
    // Material
    material = new THREE.PointsMaterial({
        size: params.size,
        sizeAttenuation: true,
        depthWrite: false,
        blending: THREE.AdditiveBlending,
        vertexColors: true
   })
    // Points
    points = new THREE.Points(geometry, material)
    // points.rotation.reorder('ZYX')
    // points.rotation.z = Math.PI/8
    scene.add(points)
}

gui.add(params, 'count').min(1).max(1_000_000).step(100).onFinishChange(generateGalaxy)
gui.add(params, 'size').min(0.001).max(0.1).step(0.001).onFinishChange(generateGalaxy)
gui.add(params, 'radius').min(0.01).max(20).step(0.01).onFinishChange(generateGalaxy)
gui.add(params, 'branches').min(2).max(20).step(1).onFinishChange(generateGalaxy)
gui.add(params, 'spin').min(-5).max(5).step(0.001).onFinishChange(generateGalaxy)
gui.add(params, 'randomness').min(0).max(2).step(0.001).onFinishChange(generateGalaxy)
gui.add(params, 'randomnessPower').min(1).max(10).step(0.001).onFinishChange(generateGalaxy)
gui.addColor(params, 'insideColor').onFinishChange(generateGalaxy)
gui.addColor(params, 'outsideColor').onFinishChange(generateGalaxy)

generateGalaxy()


/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})


/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 3
camera.position.y = 3
camera.position.z = 3
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true


/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.outputColorSpace = THREE.LinearSRGBColorSpace
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))


const tick = () =>
{
    // Update controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()


/**
 * Raycasting
 */
const raycaster = new THREE.Raycaster();
raycaster.params.Points.threshold = 0.2;
const pointer = new THREE.Vector2();

window.addEventListener('click', (event) => {
    pointer.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	pointer.y = (- ( event.clientY / window.innerHeight ) * 2 + 1);

    if (points) {
        raycaster.setFromCamera( pointer, camera );
        const intersects = raycaster.intersectObject( points );
        
        if (intersects.length) {
            const intersectedPoint = intersects[0]['point']
            const pointsPositions = points.geometry.attributes.position.array
            const pointsColors = new Float32Array(points.geometry.attributes.color.array)
            const randomColor = {
                r: Math.random(),
                g: Math.random(),
                b: Math.random()
            }

            for (let i = 0; i < pointsPositions.length/3; i++) {
                const i3 = i * 3
                
                const distance = intersectedPoint.distanceTo(new THREE.Vector3(
                    pointsPositions[i3],
                    pointsPositions[i3 + 1],
                    pointsPositions[i3 + 2]
                ))  
                
                if ( distance < params.raycastedArea) {
                    pointsColors[i3] = randomColor.r
                    pointsColors[i3 + 1] = randomColor.g
                    pointsColors[i3 + 2] = randomColor.b
                }
            }
            
            geometry.setAttribute(
                'color',
                new THREE.BufferAttribute(pointsColors, 3)
            )
        }   
    }
})

gui.add(params, 'raycastedArea').min(0.1).max(5).step(0.1)